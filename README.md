# Country App

Country App is a web application used to display the countries of the world and is developed using ReactJS.
The project implements the following:

Uses Basic Routing
Uses Local Storage for storing data
Makes use of Context API for sharing data with the components

