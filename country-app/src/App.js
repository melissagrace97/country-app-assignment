import './App.css';
import React , {useEffect, useState} from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import LoginComponent from "./modules/login/login.component"
import RegisterComponent from "./modules/register/register.component"
import ProfileContainer from './modules/profile/profile.container';
import PageNotFound from './modules/pagenotfound.component'
import { AuthContext } from './core/context/auth.context';
import { UserContext } from './core/context/user.context';
import HomeContainer from './modules/home/home.container';
import FavouriteContainer from './modules/favourite/favourite.container';

function App() {

  const [authState, setAuthState] = useState(false);
  const [user, setUser] = useState('');

  useEffect(() => {
    if(localStorage.getItem('isLoggedIn')) {
      setAuthState(true)
      setUser(localStorage.email)
    }
  }, []);

  return (
    <AuthContext.Provider value={{ authState, setAuthState }}>
      <Router>
        <Switch>
        <Route path="/" exact render={() => <Redirect to="/login" /> }/>
        <Route path="/register" component={RegisterComponent}/>
        <UserContext.Provider value={{user, setUser}}>
          <Route path="/login" component={LoginComponent}/>
          <Route path="/profile" component={ProfileContainer}/>
          <Route path="/home" component={HomeContainer}/>
          <Route path="/favourite" component={FavouriteContainer}/>
        </UserContext.Provider > 
        {/* <Route path="/**" component={PageNotFound}/>  */}
        </Switch>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;

