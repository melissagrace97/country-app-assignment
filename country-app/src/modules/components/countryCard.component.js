import React, { useState } from 'react'
import CountryDetailsComponent from './countryDetails.component';
import { IoIosHeart, IoIosHeartEmpty } from "react-icons/io";
const CountryCard = (props) => {
    const { item, processType } = props;
    const [submitted, setSubmitted] = useState(false);
    const [showCountryDetailsDialog, setShowCountryDetailsDialog] = useState(false);
    const closeCountryDetailsDialog = () => {
        setShowCountryDetailsDialog(false);
    }
    let favList = JSON.parse(localStorage.getItem('favourite')) || [];
    const [fav, setFav] = useState(favList);
    const [toggle, setToggle] = useState(false);

    const favCountry = (e) => {
        e.preventDefault();
        setSubmitted(true);
        var new_data = item;
        if (favList.find(x => x.name === new_data.name)) {
            console.log("item in list")
            setToggle(!toggle);
            const newFavList = favList.filter(x => x.name !== item.name);
            localStorage.setItem('favourite', JSON.stringify(newFavList));
        }
        else {
            favList.push(new_data);
            localStorage.setItem('favourite', JSON.stringify(favList))
            setToggle(true);
            window.location.reload(false);
        }
    }
    const removeFavCountry = () => {
        var remove_item = item;
        const newList = favList.filter(i => i.name !== item.name);
        localStorage.setItem('favourite', JSON.stringify(newList));
        window.location.reload(false);
    }
    
    return (
        <div>
            <CountryDetailsComponent {...{ setShowCountryDetailsDialog, closeCountryDetailsDialog, showCountryDetailsDialog, item }} />
            <div className="country-card" key={item.callingCodes}>
                <div className="country-card-image">
                    <img src={item.flag} alt={item.name} />
                </div>
                <div className="card-content">
                    <h2 className="card-name">{item.name}</h2>
                    <ol className="card-list">
                        <li>
                            Capital: <span>{item.capital}</span>
                        </li>
                        <li>
                            Region: <span>{item.region}</span>
                        </li>
                        <li>
                            Sub Region:{" "}
                            <span>{item.subregion}</span>
                        </li>
                    </ol>
                    <div className="country-buttons">
                        {!processType && <>
                        <button className="remove-btn" onClick={removeFavCountry}>Remove</button>
                        </>
                        }
                        {processType && <>
                            <div className="fav-btn">
                                {favList.find(x => x.name === item.name) ? (
                                    <IoIosHeart style={{ color: 'red', marginTop: 5 }}
                                        onClick={favCountry}

                                    />
                                ) : (
                                    <IoIosHeartEmpty style={{ color: 'red', marginTop: 5 }}
                                        onClick={favCountry}

                                    />
                                )
                                }
                            </div>
                        </>}
                        <div className="view-btn">
                        <button className="view-more-btn" onClick={() => setShowCountryDetailsDialog(true)}>View More</button>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CountryCard
