import React, { useState, useEffect } from 'react';
import '../../assets/css/pagination.css'
const Pagination = (props) => {
    const { countriesPerPage, totalCountries, onPaginationChange, favouriteCountry } = props;
    const [counter, setCounter] = useState(1);
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(totalCountries / countriesPerPage); i++) {
        pageNumbers.push(i);
    }
    useEffect(() => {
        const value = countriesPerPage * counter;
        onPaginationChange(value - countriesPerPage, value)
    }, [counter]);

    const onButtonClick = (type) => {
        if (type === "prev") {
            if (counter === 1) {
                setCounter(1)
            } else {
                setCounter(counter - 1)
            }
        } else if (type === "next") {
            if (Math.ceil(totalCountries / countriesPerPage) === counter) {
                setCounter(counter)
            } else {
                setCounter(counter + 1)
            }
        }
    }
    return (
        <div className="pagination-container">
            <center>
                <div className="btn-container">
                    <button style={{ marginRight: 5 }} className="btn-primary" onClick={() => onButtonClick("prev")}>Previous</button>
                    <button className="btn-primary" onClick={() => onButtonClick("next")}>Next</button>
                </div>
            </center>
        </div>
    )
}

export default Pagination;
