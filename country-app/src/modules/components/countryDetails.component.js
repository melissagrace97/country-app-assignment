import React from 'react'
import { Dialog, DialogTitle, DialogContent } from '@material-ui/core';
import '../../assets/css/countryDetails.css'
const CountryDetailsComponent = (props) => {
    const { closeCountryDetailsDialog, showCountryDetailsDialog, item } = props;
    return (
        <div>
            <Dialog open={showCountryDetailsDialog}>
                <DialogTitle>
                    <div className="country-modal-heading">
                        <div className="left-side">
                            <div>{item.name}</div>
                        </div> 
                        <div className="right-side">
                            <img src={item.flag} />
                        </div>
                    </div>
                </DialogTitle>
                <DialogContent dividers>
                    <div className="country-modal-details">
                        <div className="row">
                            <div className="left">
                                <div className="country-data">
                                    <label>Native Name: </label><span> {item.nativeName}</span>
                                </div>
                                <div className="country-data">
                                    <label>Capital: </label><span> {item.capital}</span>
                                </div>
                                <div className="country-data">
                                    <label>Region: </label><span>{item.region}</span>
                                </div>
                                <div className="country-data">
                                    <label>Sub Region: </label><span>{item.subregion}</span>
                                </div>
                                <div className="country-data">
                                    <label>Demonym: </label><span>{item.demonym}</span>
                                </div>
                            </div>
                            <div className="middle">
                                <div className="country-data-1">
                                    <label>Currencies:</label><span>{item.currencies[0].name}&nbsp;{item.currencies[0].symbol}</span>
                                </div>
                                <div className="country-data-1">
                                    <label>Area:</label><span>{item.area}</span>
                                </div>
                                <div className="country-data-1">
                                    <label>Calling Codes:</label><span>{item.callingCodes}</span>
                                </div>
                                <div className="country-data-1">
                                    <label>LatLng:</label><span>{item.latlng[0]}, {item.latlng[1]}</span>
                                </div>
                                <div className="country-data-1">
                                    <label>Population:</label><span>{item.population}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row-btn">
                        <button className="cancel-btn" onClick={closeCountryDetailsDialog}>Cancel</button>
                    </div>
                </DialogContent>
            </Dialog>
        </div>
    )
}

export default CountryDetailsComponent
