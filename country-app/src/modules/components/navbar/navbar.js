import React from 'react'
import { useHistory, NavLink} from 'react-router-dom'
import "../../../App.css"
const Navbar = (props) => {
    const history = useHistory(); 
    let isLoggedIn = localStorage.getItem('isLoggedIn');
    let user_info = JSON.parse(localStorage.getItem('user_info'));
    function logOut(e) {
        e.preventDefault();
        localStorage.removeItem('user_info');
        localStorage.removeItem('isLoggedIn');
        localStorage.removeItem('email');
        history.push('/login');
    }
    return (

        <div className="navbar">
            {user_info ? (
                <>
                    <div className="leftside">
                        <p>Country App</p>
                    </div>
                    <div className="rightside">
                        <div className="links">
                            <NavLink to="/favourite" activeClassName="text-primary">Favourite</NavLink>
                            <NavLink to="/home" activeClassName="text-primary">Home</NavLink>
                            <NavLink to="/profile" activeClassName="text-primary">Profile</NavLink>
                            <NavLink onClick={logOut} to="/login" activeClassName="text-primary">Logout</NavLink>
                        </div>
                    </div>
                </>)
                :
                (<>
                    <div className="leftside">
                        <p>Country App</p>
                    </div>
                    <div className="rightside">
                        <div className="links">
                            <NavLink to="/login" activeClassName="text-primary">Login</NavLink>
                            <NavLink to="/register" activeClassName="text-primary">Register</NavLink>
                        </div>
                    </div>
                </>)
                }
        </div>
    )
}

export default Navbar;
