import React, {useState} from 'react'
import CountryCard from '../components/countryCard.component';
import Navbar from '../components/navbar/navbar'
import Pagination from '../components/pagination';

const FavouriteContainer = (props) => {
    let favouriteCountry = JSON.parse(localStorage.getItem('favourite')) || [];
    const [countriesPerPage, setcountriesPerPage] = useState(5);
    const [pagination, setPagination] = useState({
        start: 0,
        end: countriesPerPage,
    })
    const onPaginationChange =(start, end) => {
        setPagination({start: start, end: end})
    }
     return (
        <div>
            <Navbar/>
            { favouriteCountry.length === 0 ? (
                <center>
                <h3 className="no-items">You have no favourite countries. Add countries to favourites to see it here.</h3>
                </center>
            )
            :
           ( <>
           <div>
               <center><h2 className="fav-list">Your Favourite Countries</h2></center>
                <ul className="country-fav-card-grid">
                            {favouriteCountry.slice(pagination.start, pagination.end).map((item) => (
                                <li>
                                    <CountryCard item={item}/>
                                </li>
                            ))
                        }
                </ul>
            </div>
                <Pagination countriesPerPage={countriesPerPage} totalCountries={favouriteCountry.length} onPaginationChange={onPaginationChange} favouriteCountry={favouriteCountry}/>
           </> )}
        </div>
    )
}

export default FavouriteContainer;
