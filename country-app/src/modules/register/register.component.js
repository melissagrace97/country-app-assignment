import React, { useState } from 'react'
import { InputText } from "primereact/inputtext";
import { Link, useHistory } from 'react-router-dom';
import "../../assets/css/register.css";
import userValidations from "../../utils/userValidations"

const RegisterComponent = () => {
    let history = useHistory();
    let users = JSON.parse(localStorage.getItem('users')) || [];
    const countries = require("../../country.json");
    const [registerInputs, setRegisterInputs] = useState({
        username: '',
        email: '',
        password: '',
        confirmPassword: '',
        country: ''

    });
    const [submitted, setSubmitted] = useState(false);
    const [errors, setErrors] = useState({})
    const [userExists, setUserExists] = useState(false);

    const register = () => {
        if (users.find(user => user.username === registerInputs.username) || users.find(user => user.email === registerInputs.email)) {
            setUserExists(true);
        }
        else {
            // assign user id and go to login page
            registerInputs.id = users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;
            users.push(registerInputs);
            localStorage.setItem('users', JSON.stringify(users));
            history.push("/login")
        }
    }
    const handleChange = (e) => {
        const { name, value } = e.target;
        setRegisterInputs(registerInputs => ({ ...registerInputs, [name]: value }));
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        setSubmitted(true);
        setErrors(userValidations(registerInputs))
        if (registerInputs.username && registerInputs.email && registerInputs.password && registerInputs.confirmPassword && registerInputs.country) {
            register(registerInputs);
        }
    }

    return (
        <div className="register">
            <h2 className="country-app-heading">Country App</h2>
            <div className="register_container">
                <h2>Register</h2>
                <form name="form" onSubmit={handleSubmit}>
                    {userExists && <p className="error-message">The Username or Email is already taken!</p>}
                    <center>
                        <InputText
                            type="text"
                            name="username"
                            value={registerInputs.username}
                            onChange={handleChange}
                            placeholder="Username"
                        />
                        {errors.username && <span className="font-weight-bold text-danger">*{errors.username}</span>}
                    </center>
                    <center>
                        <InputText
                            type="email"
                            name="email"
                            value={registerInputs.email}
                            onChange={handleChange}
                            placeholder="Email"
                        />
                        {errors.email && <span className="font-weight-bold text-danger">*{errors.email}</span>}
                    </center>
                    <center>
                        <InputText
                            type="password"
                            name="password"
                            value={registerInputs.password}
                            onChange={handleChange}
                            placeholder="Password"
                        />
                        {errors.password && <span className="font-weight-bold text-danger">*{errors.password}</span>}
                    </center>
                    <center>
                        <InputText
                            type="password"
                            name="confirmPassword"
                            value={registerInputs.confirmPassword}
                            onChange={handleChange}
                            placeholder="Confirm Password"
                        />
                        {errors.confirmPassword && <span className="font-weight-bold text-danger">*{errors.confirmPassword}</span>}
                    </center>
                    <div >
                        <select className="dropdown-demo" onChange={(e) => setRegisterInputs({ ...registerInputs, country: e.target.value })}>
                            <option hidden className="select-default text-gray">Select a country</option>
                            {countries.map(item => (
                                <option key={item.name} value={item.name} placeholder="Select a country">{item.name}</option>
                            ))}
                        </select>
                        {errors.country && <span className="font-weight-bold text-danger">*{errors.country}</span>}
                    </div>
                    <center>
                        <button type="submit" className="registerBtn">Register</button>
                    </center>
                    <center>
                        <div className="sideinfo">
                            <Link to="/login" style={{ textDecoration: 'none' }}>
                                <h5 className="rtd">Already have an account? Login</h5>
                            </Link>
                        </div>
                    </center>
                </form>
            </div>
        </div>
    )
}

export default RegisterComponent
