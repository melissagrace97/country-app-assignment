import { InputText } from 'primereact/inputtext';
import React, { useState, useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';
import "../../assets/css/login.css";
import userValidations from "../../utils/userValidations";
import {AuthContext} from '../../core/context/auth.context';
import {UserContext} from '../../core/context/user.context';

const LoginComponent = (props) => {
    const { setAuthState } = useContext(AuthContext);
    const { setUser } = useContext(UserContext);
    let history = useHistory();
    let users = JSON.parse(localStorage.getItem('users')) || [];
    let user_info = JSON.parse(localStorage.getItem('user_info')) || [];
    const [loginInputs, setLoginInputs] = useState({
        email: '',
        password: ''
    });
    const [submitted, setSubmitted] = useState(false);
    const [errors, setErrors] = useState({})
    const [userExists, setUserExists] = useState(false);
    const [authorized, setAuthorized] = useState(false);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setLoginInputs(loginInputs => ({ ...loginInputs, [name]: value }));
    }
    const handleSubmit = (e) => {
        e.preventDefault();   
        setSubmitted(true);
        setErrors(userValidations(loginInputs))
        if (loginInputs.email && loginInputs.password) {
            authenticateUser();

        }
    }
    const authenticateUser = () => {
        if (users.find(user => user.email === loginInputs.email && user.password === loginInputs.password)) {
            setAuthorized(true);
            localStorage.setItem('isLoggedIn', true);
            setAuthState(true);
            localStorage.setItem('user_info', JSON.stringify(loginInputs));
            localStorage.setItem('email', loginInputs.email);
            console.log(loginInputs);
            setUser(loginInputs.email);
            history.push('/home');
        } else {
            setUserExists(true)
        }
    }

    return (
        <div className="login">
            <h2 className="country-app-heading">Country App</h2>
            <div className="login_container">
                <h2>Login</h2>
                <form name="form" onSubmit={handleSubmit}>
                    {userExists && <p className="error-message">Invalid Credentials!</p>}
                    <center>
                        <InputText
                            type="email"
                            name="email"
                            placeholder="Email"
                            value={loginInputs.email}
                            onChange={handleChange}
                        />
                        {errors.email && <span className="font-weight-bold text-danger">*{errors.email}</span>}
                    </center>
                    <center>
                        <InputText
                            type="password"
                            placeholder="Password"
                            name="password"
                            value={loginInputs.password}
                            onChange={handleChange}
                        />
                        {errors.password && <span className="font-weight-bold text-danger">*{errors.password}</span>}
                    </center>
                    <center>
                        <button type="submit" className="loginBtn">
                            Log In
                         </button>
                    </center>
                    <center>
                        <div className="sideInfo">
                            <Link to="/register" style={{ textDecoration: 'none' }}>
                                <h5 className="rtd">New User? Register</h5>
                            </Link>
                        </div>
                    </center>
                </form>
            </div>
        </div>

    )
}

export default LoginComponent
