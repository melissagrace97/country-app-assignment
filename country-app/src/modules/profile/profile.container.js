import React, { useState, useContext } from 'react'
import Navbar from '../components/navbar/navbar'
import ProfileComponent from './profile.component'
import { UserContext } from '../../core/context/user.context';
import userValidations from '../../utils/userValidations';
import { useHistory } from 'react-router';
const ProfileContainer = (props) => {
    let history = useHistory();
    const { user } = useContext(UserContext);
    const countries = require("../../country.json");
    const users = JSON.parse(localStorage.getItem('users'));
    const email = localStorage.getItem('email');
    const [submitted, setSubmitted] = useState(false);
    const [errors, setErrors] = useState({})
    const [modelDialogShow, setModelDialogShow] = React.useState(false);
    const closeEditModel = () => {
        setModelDialogShow(false);
    }
    const [updatedInputs, setUpdatedInputs] = useState({
        username: '',
        password: '',
    });

    var result = users.find(x => x.email === email);
    var country = result.country;
    const username = updatedInputs.username ? updatedInputs.username : result.username;
    const password = updatedInputs.password ? updatedInputs.password : result.password;
    const confirmPassword = password;
    var country_details = countries.find(x => x.name === country);
    const handleChange = (e) => {
        const { name, value } = e.target;
        setUpdatedInputs(updatedInputs => ({ ...updatedInputs, [name]: value }));
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        setSubmitted(true);
        setErrors(userValidations(updatedInputs))
        if (updatedInputs.username && updatedInputs.password) {
            updateUser();
            setModelDialogShow(false);
        }
    }
    const updateUser = () => {
        const newState = users.map(obj =>
            obj.email === email ? { ...obj, username: username, password: password, confirmPassword: confirmPassword } : obj
        );
        localStorage.setItem('users', JSON.stringify(newState))
    }
    const deleteUser = () => {
        const newUserState = users.filter(x => x.email !== email);
        localStorage.setItem('users', JSON.stringify(newUserState));
        localStorage.removeItem('user_info');
        localStorage.removeItem('isLoggedIn');
        localStorage.removeItem('email');
        localStorage.removeItem('favourite');
        history.push('/login');
    }
    return (
        <div>
            <Navbar />
            <ProfileComponent {...{ userDetails: result, countryDetails: country_details, updatedInputs, setUpdatedInputs, handleSubmit, handleChange, errors, modelDialogShow, setModelDialogShow, closeEditModel, deleteUser }} />
        </div>
    )
}

export default ProfileContainer;