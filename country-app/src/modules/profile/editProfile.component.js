import React from 'react';
import { Dialog, DialogTitle, DialogContent } from '@material-ui/core';
import '../../assets/css/modal.css';

const EditProfileModal = (props) => {
    const { modelDialogShow, closeEditModel, userDetails, updatedInputs, handleSubmit, handleChange, errors } = props;
    return (
        <div>
            <Dialog open={modelDialogShow}>
                <DialogTitle>
                    <div> Edit Profile Details</div>
                </DialogTitle>
                <DialogContent dividers>
                    <div>
                        <form className="modal-form">
                            <div className="col">
                                <div className="row-inputs">
                                    <label>Username</label>
                                    <input type='text' placeholder={userDetails.username} name="username" value={updatedInputs.username}
                                        onChange={handleChange}
                                    />
                                    {errors.username && <span className="font-weight-bold text-danger">*{errors.username}</span>}
                                </div>
                                <div className="row-inputs">
                                    <label>Change Password</label>
                                    <input type='text' placeholder={userDetails.password} name="password" value={updatedInputs.password}
                                        onChange={handleChange}
                                    />
                                    {errors.password && <span className="font-weight-bold text-danger">*{errors.password}</span>}
                                </div>
                                <div className="row-inputs">
                                    <label>Email</label>
                                    <input disabled placeholder={userDetails.email} />
                                </div>
                                <div className="row-inputs">
                                    <label>Country</label>
                                    <input disabled type='text' placeholder={userDetails.country} />
                                </div>
                                <hr style={{ width: 100 + '%', margin: 0, padding: 0 }} />
                                <div className="row-btn">
                                    <button className="update-btn" onClick={handleSubmit}>Save Changes</button>
                                    <button className="cancel-btn" onClick={closeEditModel}>Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </DialogContent>
            </Dialog>
        </div>
    )
}

export default EditProfileModal;