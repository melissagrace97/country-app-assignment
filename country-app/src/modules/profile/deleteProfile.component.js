import React from 'react';
import { Dialog, DialogTitle, DialogContent } from '@material-ui/core';
import '../../assets/css/deleteModal.css'
const DeleteAccount = (props) => {
const {modelShow,closeDeleteModel, handleDelete} = props;
    return(
        <div>
            <Dialog open={modelShow}>
                <DialogTitle>
                    <div> Delete Account</div>
                </DialogTitle>
                <DialogContent dividers>
                    <div>
                        <form className="modal-delete">
                            <p>Are you sure you want to delete this account?</p>
                            <div className="row-btn">
                                <button className="delete-btn-1" onClick={handleDelete}>Yes</button>
                                <button className="cancel-btn-1" onClick={closeDeleteModel}>No</button>
                            </div>
                        </form>
                    </div>
                </DialogContent>
            </Dialog>
        </div>
    )
}
export default DeleteAccount;