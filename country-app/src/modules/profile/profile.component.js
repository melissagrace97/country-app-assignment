import React, { useState } from 'react'
import '../../assets/css/profile.css';
import '../../../node_modules/primeicons/primeicons.css'
import { InputText } from 'primereact/inputtext';
import EditProfileModal from './editProfile.component';
import DeleteAccount from './deleteProfile.component';

const ProfileComponent = (props) => {
    const { userDetails, countryDetails, updatedInputs, setUpdatedInputs, handleSubmit, handleChange , errors, modelDialogShow, setModelDialogShow, closeEditModel, deleteUser} = props;
    const [modelShow, setModalShow] = React.useState(false);
    const [submit, setSubmit] = useState(false);
    const closeDeleteModel = () => {
        setModalShow(false);
    }
    const handleDelete = (e) => {
        e.preventDefault(); 
        setSubmit(true);
        deleteUser()
    }
 
    return (
        <div className="profile">
           <EditProfileModal {...{modelDialogShow, setModelDialogShow, closeEditModel, userDetails, updatedInputs, setUpdatedInputs, handleSubmit, handleChange, errors}}/>
           <DeleteAccount {...{modelShow, setModalShow, closeDeleteModel, handleDelete}}/>
            <center>
                <h2 style={{fontWeight: 500}}className="heading align-center">View Profile</h2>
                <hr className="line-break" />
            </center>
            <center>
                <div className="row">
                    <div className="card-1">
                        <div className="card-container">
                            <div className="row">
                                <div className="left-side">
                                    <p>Profile Details</p>
                                </div>
                                <div className="right-side">
                                    <button className="edit-btn" onClick={() => setModelDialogShow(true)}>
                                    <i className="pi pi-user-edit" style={{ 'fontSize': '1em' }}></i> Edit Profile</button>
                                </div>
                            </div>
                            <hr className="divider" style={{ width: 100 + '%' }} />
                            <div className="row">
                                <div className="left-side-1">
                                    <img src="https://cdn1.iconfinder.com/data/icons/app-user-interface-glyph/64/user_man_user_interface_app_person-512.png" />
                                    <h2>{userDetails.username}</h2>
                                    </div>    
                                </div>
                            <div className="col">
                                <div className="profile-details">
                                    <div className="profile-data">
                                        <label>Username :</label>
                                        <InputText className="input-text" disabled value={userDetails.username}></InputText>
                                    </div>
                                    <div className="profile-data">
                                        <label>Password :</label>
                                        <InputText className="input-text" disabled type="password" value={userDetails.password}></InputText>
                                    </div>
                                    <div className="profile-data">
                                        <label>Email :</label>
                                        <InputText className="input-text-2" disabled value={userDetails.email}></InputText>
                                    </div>
                                    <div className="profile-data">
                                        <label>Country :</label>
                                        <InputText className="input-text-3" disabled value={userDetails.country}></InputText>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-2">
                        <div className="card-container">
                            <div className="row">
                                <div className="country-heading">
                                    <p>Country Details</p>
                                </div>
                            </div>
                            <hr className="divider" style={{ width: 100 + '%' }} />
                            <div className="country-details">
                                <div className="row">
                                    <div className="left-side">
                                        <img src={countryDetails.flag} />
                                        <h2 style={{marginRight: 5}}>{userDetails.country}</h2><span style={{fontWeight: 'bold'}}>({countryDetails.nativeName})</span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="left">
                                        <div className="country-data">
                                            <label>Capital: </label><span> {countryDetails.capital}</span>
                                        </div>
                                        <div className="country-data">
                                            <label>Region: </label><span>{countryDetails.region}</span>
                                        </div>
                                        <div className="country-data">
                                            <label>Sub Region: </label><span>{countryDetails.subregion}</span>
                                        </div>
                                    </div>
                                    <div className="middle">
                                        <div className="country-data-1">
                                            <label>Demonym: </label><span>{countryDetails.demonym}</span>
                                        </div>
                                        <div className="country-data-1">
                                            <label>Currencies:</label><span>{countryDetails.currencies[0].name}&nbsp;{countryDetails.currencies[0].symbol}</span>
                                        </div>
                                        <div className="country-data-1">
                                            <label>Area:</label><span>{countryDetails.area}</span>
                                        </div>
                                        <div className="country-data-1">
                                        </div>
                                    </div>
                                    <div className="right">
                                        <div className="country-data-1">
                                            <label>Calling Codes:</label><span>{countryDetails.callingCodes}</span>
                                        </div>
                                        <div className="country-data-1">
                                            <label>LatLng:</label><span>{countryDetails.latlng[0]}, {countryDetails.latlng[1]}</span>
                                        </div>
                                        <div className="country-data-1">
                                            <label>Population:</label><span>{countryDetails.population}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="time-zone">
                                    <label>Time Zones:</label><span>{countryDetails.timezones}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </center>
            <div className="delete-account">
                <button className="delete-btn" onClick={() => setModalShow(true)}>Delete Account</button>
            </div>
        </div>
    )
}

export default ProfileComponent;
