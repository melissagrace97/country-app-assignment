import React from 'react'
import '../../assets/css/home.css'
import CountryCard from '../components/countryCard.component';
import Pagination from '../components/pagination'
const HomeComponent = (props) => {
    const { setFilterRegion, region, countries, search, countriesPerPage, pagination, onPaginationChange} = props;
    const processType = Object.freeze({home:1});
    return (
        <div>
        <div className="country-container">
            <div className="filter-region">
                <select className="drop-down"
                    onChange={(e) => {
                        setFilterRegion(e.target.value);
                    }}
                    aria-label="Filter Countries By Region">
                    <option value="All">Filter By Region</option>
                    {region.map(item => (
                        <option>{item}</option>
                    ))}
                </select>
                <span className="focus"></span>
            </div>
            <div>
            <ul className="country-card-grid">
                        {search(countries).slice(pagination.start, pagination.end).map((item) => (
                            <li>
                                <CountryCard {...{item, processType}}/>
                            </li>
                        ))
                    }
                 </ul>
            </div>
          
        </div>
        <Pagination countriesPerPage={countriesPerPage} totalCountries={search(countries).length} onPaginationChange={onPaginationChange}/>
        </div>

    )
}

export default HomeComponent




    

    
      



