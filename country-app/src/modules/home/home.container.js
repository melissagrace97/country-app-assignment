import React, {useState} from 'react';
import Navbar from '../components/navbar/navbar';
import HomeComponent from './home.component';

const HomeContainer = (props) => {
    const [filterRegion, setFilterRegion] = useState(["All"]);
    const countries = require("../../country.json");
    const region = [...new Set(countries.map(item => item.region))];
    const [searchParam] = useState(["name"]);
    const [countriesPerPage, setCountriesPerPage] = useState(5);
    const [pagination, setPagination] = useState({
        start: 0,
        end: countriesPerPage,
    })
    const onPaginationChange =(start, end) => {
        setPagination({start: start, end: end})
    }
    function search(countries) {
        return countries.filter((item) => {
            if (item.region == filterRegion) {
                return searchParam.some((newItem) => {
                    return (item[newItem]);
                });
            } else if (filterRegion == "All") {
                return searchParam.some((newItem) => {
                    return (item[newItem]);
                });
            }
        });
    }
    return (
        <div>
            <Navbar/>
            <HomeComponent {...{region, filterRegion, setFilterRegion, countries, search, countriesPerPage, pagination, onPaginationChange}}/>
        </div>
    )
}

export default HomeContainer
   