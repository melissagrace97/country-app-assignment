import React from 'react';
import { Link } from 'react-router-dom';
const PageNotFound = () => {
    return (
        <div>
            <center>
            <h3>404 - Page Not Found</h3>
            <Link to="/">Back to Home</Link>
            </center>
           
        </div>
    )
}
export default PageNotFound;