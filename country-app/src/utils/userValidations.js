export default function userValidations(values) {
    let errors = {}

    if(!values.username){
        errors.username = "Username is required"
    }else if( /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/i.test(values.username)){
        errors.username ="Username should not contain any special characters"
    }

    if(!values.email) {
        errors.email = "Email is required"
    }else if(!/^[A-Za-z0-9._\%+-]+@[A-Za-z0-9.\-]+\.[A-Za-z]{2,4}$/i.test(values.email)){
        errors.email = "Email address is invalid"
    }

    if(!values.password) {
        errors.password = "Password is required"
    }else if(values.password.length < 6 || values.password.length > 15){
        console.log(values.password);
        errors.password = "Password needs to be between 6-15 characters "
    }else if( /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/i.test(values.password)){
        errors.password ="Password should not contain any special characters"
    }

    if(!values.confirmPassword) {
        errors.confirmPassword = "Password is required"
    }else if(values.confirmPassword !== values.password){
        errors.confirmPassword = "Passwords do not match"
    }else if((values.password === values.confirmPassword) && (values.confirmPassword.length < 6 || values.confirmPassword.length > 15)){
        errors.confirmPassword = "Password needs to be between 6-15 characters "
    }else if( /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/i.test(values.confirmPassword)){
        errors.confirmPassword ="Password should not contain any special characters"
    }

    if(!values.country) {
        errors.country = "Country is required"
    }
    return errors;
}